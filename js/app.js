const formularioContactos = document.querySelector('#contacto'),
      listadoContactos = document.querySelector('#listado-contactos'),
      inputBuscador = document.querySelector('#buscar');

eventListeners();

function eventListeners() {

    // * WHEN CREATE OR EDIT FORM IT IS EXECUTED
    
    formularioContactos.addEventListener( 'submit', leerFormulario );

    // LISTENER TO DELETE BUTTON
    if ( listadoContactos ){
        listadoContactos.addEventListener( 'click', eliminarContacto );
    }
    

    // SEARCH
    inputBuscador.addEventListener('input', buscarContactos);

    numeroContactos();

}

function leerFormulario(e) {

    e.preventDefault();

    // * READ DATA FROM INPUTS
    const nombre = document.querySelector('#nombre').value,
          empresa = document.querySelector('#empresa').value,
          telefono = document.querySelector('#telefono').value,
          accion = document.querySelector('#accion').value;

    // console.log(nombre, empresa, telefono);

    if ( 
        nombre === '' || nombre === undefined ||
        empresa === '' || empresa === undefined ||
        telefono === '' || telefono === undefined 
    ) {

        // TWO PARAMETERS TEXT AND CLASS
        mostrarNotificacion('Todos los campos son obligatorios', 'error');

    } else {

        // PASS VALIDATION, CREATE AJAX CALL
        const infoContacto = new FormData();
        infoContacto.append('nombre', nombre);
        infoContacto.append('empresa', empresa);
        infoContacto.append('telefono', telefono);
        infoContacto.append('accion', accion);

        // console.log(...infoContacto);

        if ( accion === 'crear' ) {

            // CREATE NEW CONTACT
            insertarBD( infoContacto );

        } else {

            // EDIT CONTACT
            const idRegistro = document.querySelector('#id').value;
            infoContacto.append( 'id', idRegistro );
            actualizarRegistro( infoContacto );

        }


    }


}

/** INSERT DATA ON THE BD WITH AJAX */
function insertarBD(datos) {

    // CALL TO AJAX

    // CREATE THE OBJECT
    const xhr = new XMLHttpRequest();

    // OPEN CONNECTION
    xhr.open('POST','inc/modelos/modelo-contactos.php', true );

    // SET DATA
    xhr.onload = function() {

        if( this.status === 200 ) {
            
            console.log( JSON.parse(xhr.responseText) );

            // READ PHP RESPONSE
            const respuesta = JSON.parse(xhr.responseText);

            // INSERT A NEW CONTACT ON THE TABLE
            const nuevoContacto = document.createElement('tr');

            nuevoContacto.innerHTML = `
                <td>${respuesta.datos.nombre}</td>
                <td>${respuesta.datos.empresa}</td>
                <td>${respuesta.datos.telefono}</td>
            `;

            // CREATE CONTAINER FOR THE BUTTONS
            const contenedorAcciones = document.createElement('td');

            // CREATE EDIT ELEMENT
            // * ---------------------------------------------------- * //
            const iconoEditar = document.createElement('i');
            // ADD STYLE FOR THE ICON
            iconoEditar.classList.add('fas', 'fa-pen-square');

            // CREATE LINK PROPERTY TO EDIT
            const btnEditar = document.createElement('a');
            // ADD EDIT ICON TO THE BUTTON
            btnEditar.appendChild(iconoEditar);
            // ADD LINK ADDRESS TO THE CONTACT
            btnEditar.href = `editar.php?id=${respuesta.datos.id_insertado}`;
            // ADD STYLE TO THE EDIT BUTTON
            btnEditar.classList.add( 'btn', 'btn-editar' );

            // ADD EDIT BUTTON TO ITS CONTAINER
            contenedorAcciones.appendChild(btnEditar);

            // * ---------------------------------------------------- * //

            // CREATE DELETE ELEMENT
            // * ---------------------------------------------------- * //
            const iconoEliminar = document.createElement('i');
            // ADD STYLE FOR THE ICON
            iconoEliminar.classList.add('fas', 'fa-trash-alt');

            // CREATE BUTTON TO DELIT
            const btnEliminar = document.createElement('button');
            // ADD EDIT ICON TO THE BUTTON
            btnEliminar.appendChild(iconoEliminar);
            btnEliminar.setAttribute('data-id', respuesta.datos.id_insertado);
            // ADD STYLE TO THE EDIT BUTTON
            btnEliminar.classList.add( 'btn', 'btn-borrar' );

            // ADD EDIT BUTTON TO ITS CONTAINER
            contenedorAcciones.appendChild(btnEliminar);

            // * ---------------------------------------------------- * //

            // ADD CONTAINER TO THE TABLE ROW
            nuevoContacto.appendChild(contenedorAcciones);

            // ADD ELEMENT TO THE TABLE
            listadoContactos.appendChild(nuevoContacto);

            // RESET FORM
            document.querySelector('form').reset();

            mostrarNotificacion( 'Contacto Creado Correctamente', 'correcto' );

            numeroContactos();

        }
    }

    // SEND DATA
    xhr.send( datos );
}

function actualizarRegistro( datos ){

    // CREATE THE OBJECT
    const xhr = new XMLHttpRequest;

    // OPEN CONNECTION
    xhr.open('POST', 'inc/modelos/modelo-contactos.php', true);

    // READ THE RESPONSE
    xhr.onload = function() {

        if( this.status === 200 ) {

            const resultado = JSON.parse( xhr.responseText );

            console.log(resultado);

            if ( resultado.respuesta === 'correcto' ) {

                mostrarNotificacion( 'Contacto Editado Correctamente', 'correcto' );

            } else {

                mostrarNotificacion( 'Hubo un error', 'error' );

            }

            setTimeout( () => {

                window.location.href = 'index.php';

            }, 4000);
            
        }

    }

    // SEND DATA
    xhr.send( datos );

}

function eliminarContacto(e) {

    if ( e.target.parentElement.classList.contains('btn-borrar') ) {

        // TAKE THE ID
        const id = e.target.parentElement.getAttribute('data-id');

        console.log(id);

        const respuesta = confirm('¿Estás seguro(a)?');

        if ( respuesta ) {

            // CREATE THE OBJECT
            const xhr = new XMLHttpRequest;

            // OPEN THE CONECTION
            xhr.open('GET', `inc/modelos/modelo-contactos.php?id=${id}&accion=borrar`, true );


            // READ THE RESPONSE
            xhr.onload = function() {

                if( this.status === 200 ) {

                    const resultado = JSON.parse(xhr.responseText);

                    console.log( resultado );

                    if( resultado.respuesta === 'correcto' ) {

                        // DELETE ELEMENT FROM DOM
                        e.target.parentElement.parentElement.parentElement.remove();

                        // SHOW NOTIFICATION
                        mostrarNotificacion('Contacto eliminado', 'correcto' );

                        numeroContactos();

                    } else {

                        // SHOW A NOTIFICATION

                        mostrarNotificacion('Hubo un error', 'error');

                    }

                }
            }

            // SEND REQUEST
            xhr.send();


        } else {

            console.log('Dejame pensarlo más');

        }

    }

}


/**
 * 
 * @param {string} mensaje 
 * @param {string} clase 
 */

// SCREEN NOTIFICATION
function mostrarNotificacion(mensaje, clase) {

    const notificacion = document.createElement('div');

    notificacion.classList.add(clase, 'notificacion', 'sombra');

    notificacion.textContent = mensaje;

    //  FORM
    formularioContactos.insertBefore( notificacion, document.querySelector('form legend') );

    // HIDE OR SHOW NOTIFICATION
    setTimeout( () => {

        notificacion.classList.add('visible');

        setTimeout( () => {

            notificacion.classList.remove('visible');

            setTimeout( () => { notificacion.remove(); }, 500 )
            

        }, 3000 );

    }, 100);

}

// SEARCH ITEMS
function buscarContactos(e) {

   const expression = new RegExp( e.target.value, 'i' ),
         registros = document.querySelectorAll('tbody tr');

    registros.forEach( registro => {
        
        registro.style.display = 'none';

        if ( registro.childNodes[1].textContent.replace(/\s/g, " ").search(expression) != -1 ) {

            registro.style.display = 'table-row';

        }

        numeroContactos();

    });

}

// SHOW THE NUMBER OF ITEMS
function numeroContactos() {

    const totalContactos = document.querySelectorAll('tbody tr'),
          contenedorNumero = document.querySelector('.total-contactos span');

    // console.log(totalContactos.length);
    let total = 0;

    totalContactos.forEach(contacto => {
        
        // console.log(contacto.style.display);

        if (contacto.style.display === '' ||  contacto.style.display === 'table-row' ) {

            total++;

        }

        contenedorNumero.textContent = total;

    });

}