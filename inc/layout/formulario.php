<?php $url = $_SERVER['PHP_SELF']; ?>

<div class="campos">

    <div class="campo">
        <label for="nombre" class="nombre">Nombre : </label>
        <?php if ($url == '/agenda-php/editar.php') { ?>
        <input 
                type="text"
                placeholder="Nombre Contacto"
                name="nombre"
                id="nombre"
                value="<?php echo ($contacto['nombre']) ? $contacto['nombre'] : ''; ?>"
        />
        <?php } else { ?>
            <input type="text" placeholder="Nombre Contacto" id="nombre" name="nombre">
        <?php } ?>
    </div>

    <div class="campo">
        <label for="empresa" class="empresa">Empresa : </label>
        <?php if ($url == '/agenda-php/editar.php') { ?>
        <input 
                type="text"
                placeholder="Empresa Contacto"
                name="empresa"
                id="empresa"
                value="<?php echo ($contacto['empresa']) ? $contacto['empresa'] : ''; ?>"
        />
        <?php } else { ?>
            <input type="text" placeholder="Empresa Contacto" name="empresa" id="empresa" />
        <?php } ?>
        
    </div>

    <div class="campo">
        <label for="telefono" class="nombre">Tel&eacute;fono : </label>
        <?php if ($url == '/agenda-php/editar.php') { ?>
        <input 
                type="tel"
                placeholder="Tel&eacute;fono Contacto"
                name="telefono"
                id="telefono"
                value="<?php echo ($contacto['telefono']) ? $contacto['telefono'] : ''; ?>"
        />
        <?php } else { ?>
            <input type="text" placeholder="Tel&eacute;fono Contacto" name="telefono" id="telefono" />
        <?php } ?>
    </div>

</div>

<div class="campo enviar">
    <?php
        $textoBtn = ($url == '/agenda-php/editar.php') ? 'Guardar' : 'Añadir';
        $accion = ($url == '/agenda-php/editar.php') ? 'editar' : 'crear';
    ?>
    <input type="hidden" id="accion" value="<?php echo $accion; ?>">
    <?php if ( isset($contacto['id']) ) { ?>
          <input type="hidden" id="id" value="<?php echo $contacto['id']; ?>">
     <?php } ?>      
    <input type="submit" value="<?php echo $textoBtn; ?>">
</div>